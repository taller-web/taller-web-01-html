<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">

  <title>Ensalada</title>
  <style type="text/css" media="screen">
body{
  font-family: sans-serif;
}
  </style>
  <script type="text/javascript" charset="utf-8">
    alert('Cargando');
  </script>
</head>
<body style="background: #cda;" onload="alert('Sitio cargado');">
  
<?php
$conexion = mysql_connect("localhost", "mysql_user", "mysql_password");

if (!$conexion) {
    echo "No pudo conectarse a la BD: " . mysql_error();
    exit;
}

if (!mysql_select_db("nombre_de_la_bd")) {
    echo "No ha sido posible seleccionar la BD: " . mysql_error();
    exit;
}

$sql = "SELECT id as id_usuario, nombre_completo, status_usuario
        FROM   alguna_tabla
        WHERE  status_usuario = 1";

$resultado = mysql_query($sql);

if (!$resultado) {
    echo "No pudo ejecutarse satisfactoriamente la consulta ($sql) " .
         "en la BD: " . mysql_error();
    exit;
}

if (mysql_num_rows($resultado) == 0) {
    echo "No se han encontrado filas, nada a imprimir, asi que voy " .
         "a detenerme.";
    exit;
}

// Mientras exista una fila de datos, colocar esa fila en $fila
// como un array asociativo
// Nota: Si solo espera una fila, no hay necesidad de usar un ciclo
// Nota: Si coloca extract($fila); dentro del siguiente ciclo,
//       estará creando $id_usuario, $nombre_completo, y $status_usuario

while ($fila = mysql_fetch_assoc($resultado)) {
    echo $fila["id_usuario"];
    echo $fila["nombre_completo"];
    echo $fila["status_usuario"].'<br />';
}

mysql_free_result($resultado);

?>

Copyright <?php echo date('Y');?>
</body>
</html>
