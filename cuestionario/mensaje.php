<?php
session_start();
$mensaje='';
if(isset($_SESSION['mensaje'])){
  $mensaje=$_SESSION['mensaje'];
  unset($_SESSION['mensaje']);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Cuestionario</title>
    <link rel="stylesheet" href="css/master.css" type="text/css" media="screen" />
  </head>
  <body>
    <div id="container">
      <h1>¿Qué les pareció el taller?</h1>
      <h3><?php echo $mensaje;?></h3>
      <a href="./" title="Volver">Volver</a>
    </div>
  </body>
</html>
