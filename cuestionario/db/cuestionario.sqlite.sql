BEGIN;
CREATE TABLE "respuesta"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "nombre" VARCHAR(255) NOT NULL,
  "email" VARCHAR(255) NOT NULL,
  "expectativas" INTEGER NOT NULL,
  "comentarios_expectativas" TEXT NOT NULL,
  "instructor" INTEGER NOT NULL,
  "comentarios_instructor" TEXT NOT NULL,
  "temas" INTEGER NOT NULL,
  "comentarios_temas" TEXT NOT NULL,
  "claridad" INTEGER NOT NULL,
  "comentarios_claridad" TEXT NOT NULL,
  "comentarios" TEXT NOT NULL
);
COMMIT;
