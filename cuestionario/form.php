<?php
session_start();
// Esto requiere, PHP, PDO, Sqlite y PDO_Sqlite

$params=array('nombre','email','expectativas','comentarios_expectativas','instructor','comentarios_instructor','temas','comentarios_temas','claridad','comentarios_claridad','comentarios');

$mensaje='Error al grabar la información del cuestionario';

$ret=FALSE;

if (isset($_COOKIE['voted'])){
  $mensaje='¿Dos veces el cuestionario? Ni lo pienses :D';
}
elseif(isset($_POST[$params[0]])){
  $db = new PDO('sqlite:./db/cuestionario.db');
  
  $sql1=$sql2='';
  foreach ($params as $key){
    $sql1.=','.$key;
    $val=isset($_POST[$key])?$_POST[$key]:'';
    $sql2.=',"'.sanitize($val).'"';
  }
  if ($sql1!=''){
    $sql='INSERT INTO respuesta ('.substr($sql1,1).') VALUES ('.substr($sql2,1).')';
    $ret=$db->exec($sql);
    if($ret!==FALSE){
      echo 
      $mensaje='Se ha enviado correctamente el cuestionario';
      setcookie('voted',1);
    }
    else{
      var_dump($db->errorInfo());
      die;
    }
  }
}

$_SESSION['mensaje']=$mensaje;
redirect('mensaje.php');

function sanitize($str){
  // si quieren agregarle mas seguridad, allá ustedes...
  return stripslashes(htmlspecialchars($str));
}

function redirect($url){
  header('Location: '.$url);
}
